[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil)

If you want a server setup for the Any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

## ![](http://i.imgur.com/pxxEKBa.png)

## Bad Wither No Cookie!

A lightweight universal mod to silence these nasty world wide broadcast sounds:

*   Wither
*   End Dragon
*   Thunder
*   and now any sound that any mod makes!

This is especially handy on servers where spawners and/or farms are in use for these mobs. This mod can be installed on a client even if it is not on the server.

## Features

Block wither spawn, end dragon death, lightning, thunder, other sounds you care to configure

\[1.16.1+\] also blocks wither death, wander\_trader and llama ambient sounds as well as wither death

### Command

*   \[1.15.2 and lower\] /listen - this is a toggle command that allows you to see the name of the event that is playing the sound. You can use these event names in the config file to silence the sound in your pack.

### Backporting

Also, I don't believe that I'll port this down below 1.9.4, I tried and the event system was no where near the same. 1.9.4 had me do some hurdles but it was doable, 1.8.9 and 1.7.10 were complete disasters. I'm gonna have learn more about those Forge versions to be able to do it.

### Implemented Features:

*   config file
*   all sound types mentioned in this overview disabled by default
*   console output when a sound is silenced also silenced by default
*   command to see event sound names: /listen

### Licenses

Code, Textures and binaries are licensed under the MIT License.

You are allowed to use the mod in your modpack.

Any alternate licenses are noted where appropriate.

## Help a Veteran today

I am Veteran of United States Army. I am not disabled. But I do love to make these mods and modpacks for you guys. Please help me to help you by Donating at [https://patreon.com/kreezxil](https://patreon.com/kreezxil)
